-- ~/.config/wireshark/plugins/ for OSX

p_proto = Proto("lnet", "LinkNetwork")
p_proto.fields = {
        ProtoField.string("lnet.dst", "destination"),
        ProtoField.string("lnet.src", "source"),
        ProtoField.uint8("lnet.version", "version"),
        ProtoField.string("lnet.orig", "originator"),
        ProtoField.uint8("lnet.action", "action"),
        ProtoField.string("lnet.comp", "compression"),
        ProtoField.uint8("lnet.r1", "reserved1"),
        ProtoField.uint8("lnet.r2", "reserved2"),
        ProtoField.uint8("lnet.r3", "reserved3"),
        ProtoField.string("lnet.src_domain", "source_domain"),
        ProtoField.string("lnet.src_name", "source_name"),
        ProtoField.ipv4("lnet.pr_ipv4", "private_ipv4"),
        ProtoField.ipv4("lnet.pu_ipv4", "public_ipv4"),
        ProtoField.uint16("lnet.pr_port", "private_port"),
        ProtoField.uint16("lnet.pu_port", "public_port")
    }
local titles = {
    "Destination",
    "Source",
    "Version",
    "From Broker",
    "Action",
    "Compression",
    "Reserved 1",
    "Reserved 2",
    "Reserved 3",
    "Source Domain",
    "Source Name",
    "Private IPv4",
    "Public IPv4",
    "Private Port",
    "Public Port"
}
local actions = {
    "ACTION PACKET",
    "ACTION REGISTER",
    "ACTION DEREGISTER",
    "ACTION REGISTER REQ",
    "ACTION REGISTERION ACK",
    "ACTION REGISTER NACK",
    "ACTION PROBE",
    "ACTION BROKER KEY REQ",
    "ACTION BROKER KEY ACK",
    "ACTION BROKER KEY NACK",
    "ACTION ANNOUNCE",
    "ACTION ANNOUNCE ACK",
    "ACTION DIRECT ACK",
    "ACTION SUBSCRIBE",
    "ACTION UNSUBSCRIBE",
    "ACTION CONFIG UPDATE REQ",
    "ACTION CONFIG UPDATE NACK"
}

function get_action_from_buffer(typ)
    return actions[typ+1]
end

function p_proto.dissector(buffer, pinfo, tree)
    local version = buffer(12,1):uint()
    local action = buffer(14,1):int()
    if (version ~= 2 or action > 16 or action < 0) then
        return 0
    end
    local action_str = get_action_from_buffer(action)

    local from_brk = ""
    if buffer(13,1):int() ~= 0 then
        from_brk = "Yes"
    else
        from_brk = "No"
    end
    local compress = ""
    if buffer(15,1):int() ~= 0 then
        compress = "Enabled"
    else
        compress = "Disabled"
    end

    pinfo.cols.protocol = "LNET"
    local subtree = tree:add(p_proto, buffer(), "Link Network Header, " .. action_str)
    local values = {
        tostring(buffer(0, 6):ether()),
        tostring(buffer(6, 6):ether()),
        version,
        from_brk,
        action,
        compress,
        buffer(16, 1):uint(),
        buffer(17, 1):uint(),
        buffer(18, 1):uint(),
        buffer(19, 32):stringz(),
        buffer(51, 32):stringz(),
        buffer(83, 4):ipv4(),
        buffer(87, 4):ipv4(),
        buffer(91, 2):le_uint(),
        buffer(93, 2):le_uint()
    }

    for i = 1, 15 do
        subtree:add(p_proto.fields[i], values[i])
               :set_text(titles[i] .. ": " .. tostring(values[i]))
    end
end

udp_table = DissectorTable.get("udp.port")
udp_table:add(7719, p_proto)
udp_table:add(7718, p_proto)